'use strict'

const connect = require('connect')
const app = connect()
const http = require('http')
const infra = require('./infra')
require('./app/setup')(app, infra)

http.createServer(app).listen(infra.port, () => {
  console.log(`Server listening on http://localhost:${infra.port}`)
})
