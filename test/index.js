'use strict'

process.env.MOCK_STUBS = 'dynamoose'
const test = require('tape')
const connect = require('connect')
const app = connect()
const infra = require('../infra')
require('../app/setup')(app, infra)

require('./ping')(app)
require('./service')(app)

test.onFinish(function () {
  process.exit(0)
})
