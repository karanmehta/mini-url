const utility = require('../../../app/libs/utility')
const reject = () => {}
const mockReq = {
  headers: {}
}
const mockRes = {
  setHeader: (key, value) => {},
  end: () => {}
}
const error = new Error('Error')
describe('Utility', () => {
  it('Unit testing utility methods', () => {
    expect(utility.createError('403', 'abc', 'no message', error)).toBeDefined()
    expect(utility.promiseTimeout(0, reject, 'Request timed out!!')).toBeDefined()
    expect(utility.getTableId()).toBeDefined()
    expect(utility.sendResponse(null, 200, {}, mockReq, mockRes)).toBeUndefined()
    expect(utility.sendResponse(null, 201, {}, mockReq, mockRes)).toBeUndefined()
    expect(utility.sendResponse(null, 202, {}, mockReq, mockRes)).toBeUndefined()
    expect(utility.sendResponse(null, 204, {}, mockReq, mockRes)).toBeUndefined()
    expect(utility.sendResponse(null, 400, {}, mockReq, mockRes)).toBeUndefined()
    expect(utility.sendResponse(null, 403, {}, mockReq, mockRes)).toBeUndefined()
    expect(utility.sendResponse(null, 404, {}, mockReq, mockRes)).toBeUndefined()
    expect(utility.sendResponse(null, 409, {}, mockReq, mockRes)).toBeUndefined()
  })
})
