'use strict'

module.exports = (infra) => {
  const db = require('../libs/db')(infra)
  const referenceSchema = new db.library.Schema({
    id: {
      type: String,
      required: true,
      unique: true,
      hashKey: true
    }, // Primary Key
    mapping: {
      type: String,
      required: true,
      index: {
        global: true
      }
    }
  }, {
    useDocumentTypes: true
  })

  const reference = db.library.model(`${infra.db.dbEnv}-Reference`, referenceSchema, {
    create: infra.db.create,
    update: infra.db.update
  })
  return reference
}
