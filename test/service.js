'use strict'

let test = require('tape')
let request = require('supertest')
const payload = {
  url: 'http://test.g.com'
}
let id = null

module.exports = (app) => {
  test('Generate short ID test', (t) => {
    request(app)
      .post(`/api/miniurl`)
      .send(payload)
      .expect(201)
      .expect('Content-Type', /json/)
      .end(function (err, res) {
        id = res && res.body && res.body.shortId ? res.body.shortId : null
        t.error(err, 'Passed')
        t.end()
      })
  })

  test('Retrieve url', (t) => {
    request(app)
      .get(`/api/miniurl/${id}`)
      .expect(200)
      .expect('Content-Type', /json/)
      .end(function (err, res) {
        t.error(err, 'Passed')
        t.end()
      })
  })

  test('Redirect URL', (t) => {
    request(app)
      .get(`/${id}`)
      .expect(302)
      .end(function (err, res) {
        t.error(err, 'Passed')
        t.end()
      })
  })
}
