'use strict'

const shortId = require('shortid')
const http = require('http')

class Utility {
  createError(code, name, message, err) {
    let error = new Error(message)
    error.name = name
    error.code = code
    if (err && err.stack) {
      error.stack = err.stack
    }
    return error
  }
  promiseTimeout(time, reject, errorMessage) {
    let timeout = setTimeout(() => {
      clearTimeout(timeout)
      reject(this.createError(500, 'Timeout', `Request Timed out: ${errorMessage}`))
    }, time)
    return timeout
  }
  getTableId () {
    return shortId.generate()
  }
  sendResponse(error, statusCode, obj, req, res) {
    res.statusCode = statusCode
    if (error) {
      res.setHeader('Content-Type', 'application/json')
      res.end(JSON.stringify({errors: [{status: res.statusCode, title: error.name, message: error.message}]}))
    } else {
      switch (statusCode) {
        case 200:
        case 201:
        case 500:
          res.setHeader('Content-Type', 'application/json')
          res.end(JSON.stringify(obj || {}, null, 2))
          break
        case 202:
        case 204:
          res.end()
          break
        case 302:
          res.writeHead(302, obj)
          res.end()
          break
        case 400:
        case 403:
        case 404:
        case 409:
        default:
          res.setHeader('Content-Type', 'application/json')
          res.end(JSON.stringify({errors: [{status: statusCode, title: http.STATUS_CODES[statusCode]}]}, null, 2))
          break
      }
    }
  }
}

module.exports = new Utility()
