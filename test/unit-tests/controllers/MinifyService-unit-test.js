'use strict'
process.env.MOCK_STUBS = 'dynamoose'
const infra = require('../../../infra')
const minifyController = require('../../../app/controllers/MinifyService')(infra)
const randomUrl = require('random-url')

const ramdonReq = { swagger: { params: { body: { value: { url: randomUrl() } } } } }
const request = { swagger: { params: { body: { value: { url: 'http://unit-test.org' } } } } }
const errorReq = { swagger: { params: { body: { value: { url: '' } } } } }
let retrieveReq = {}
let req = ramdonReq
let response = {
  jsonString: '',
  kvmap: [],
  redirectHead: [],
  setHeader: function(key, value) {
    this.kvmap[key] = value
  },
  writeHead: function(code, value) {
    this.redirectHead = value
  },
  end: function(jsonString) {
    this.jsonString = jsonString
  }
}
describe('Minify controller', () => {
  beforeEach((done) => {
    minifyController.generateShortId(req, response, () => {})
    setTimeout(() => {
      done()
    }, 200)
  })
  it('Unit testing for shortId generation for randon URL', () => {
    let json = JSON.parse(response.jsonString)
    req = errorReq
    expect(response.kvmap['Content-Type']).toBe('application/json')
    expect(response.statusCode).toBe(201)
    expect(json.shortId).toBeDefined()
  })
  it('Unit testing for shortId generation with error request', () => {
    let json = JSON.parse(response.jsonString)
    req = request
    expect(response.kvmap['Content-Type']).toBe('application/json')
    expect(response.statusCode).toBe(400)
    expect(json.shortId).toBeUndefined()
  })
  it('Unit testing for shortId generation', () => {
    let json = JSON.parse(response.jsonString)
    retrieveReq = { swagger: { params: { shortId: { value: json.shortId } } } }
    expect(response.kvmap['Content-Type']).toBe('application/json')
    expect(response.statusCode).toBe(201)
    expect(json.shortId).toBeDefined()
  })
})
describe('Minify controller', () => {
  beforeEach((done) => {
    console.log(retrieveReq)
    minifyController.redirectTo(retrieveReq, response, () => {})
    setTimeout(() => {
      done()
    }, 200)
  })
  it('Unit testing for redirecting', () => {
    console.log(response)
    expect(response.statusCode).toBe(302)
  })
})
describe('Minify controller', () => {
  beforeEach((done) => {
    minifyController.retrieveUrl(retrieveReq, response, () => {})
    setTimeout(() => {
      done()
    }, 200)
  })
  it('Unit testing for retreiving url', () => {
    let json = JSON.parse(response.jsonString)
    retrieveReq = { swagger: { params: { shortId: { value: '1111' } } } }
    expect(response.kvmap['Content-Type']).toBe('application/json')
    expect(response.statusCode).toBe(200)
    expect(json.url).toBeDefined()
  })
  it('Unit testing for retreiving url using wrong ID', () => {
    let json = JSON.parse(response.jsonString)
    expect(response.kvmap['Content-Type']).toBe('application/json')
    expect(response.statusCode).toBe(404)
    expect(json.url).toBeUndefined()
  })
  afterAll(() => setTimeout(() => process.exit(), 1000))
})
