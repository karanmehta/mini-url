FROM node:8-alpine

# build environment argument
ARG BUILD_ENV

# setting node environment before build
ENV NODE_ENV=$BUILD_ENV

RUN mkdir -p /usr/src/miniurl

# Create app directory
WORKDIR /usr/src/miniurl

# copy package & package-lock
COPY package.json /usr/src/miniurl

# Install dependencies
RUN npm install

# Bundle app source
COPY . /usr/src/miniurl

#Expose to the API port
EXPOSE 8083

#Starting the service
CMD export PORT=8083 && npm start
