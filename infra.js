'use strict'
const utility = require('./app/libs/utility')

const infra = {
  port: process.env.PORT || 8083,
  awsRegion: 'ap-southeast-2',
  logger: {
    srcFlag: true,
    outputMode: process.env.LOG_OUTPUT_MODE || 'json',
    level: process.env.LOG_LEVEL || 'debug'
  },
  db: {
    create: true,
    update: true,
    timeout: 5000,
    dbEnv: process.env.AWS_ENV || 'local'
  },
  utility: utility
}
if (process.env.MOCK_STUBS && process.env.MOCK_STUBS.indexOf('dynamoose') !== -1) {
  infra.db.local = true
  infra.db.port = 8000
  infra.db.host = process.env.DB_HOST || 'localhost'
}
const logger = require('./app/libs/logger')(infra)
infra.logger = logger
module.exports = infra
