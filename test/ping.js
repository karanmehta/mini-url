'use strict'

const test = require('tape')
const request = require('supertest')

module.exports = (app) => {
  test('Ping test', (t) => {
    request(app)
      .get('/api/ping')
      .expect(200)
      .expect({
        message: 'server running'
      })
      .expect('Content-Type', /json/)
      .end(function (err, res) {
        t.error(err, 'Passed')
        t.end()
      })
  })
}
