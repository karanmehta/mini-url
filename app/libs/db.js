'use strict'

class ModelImplementation {
  constructor(infra, Model) {
    this.infra = infra
    this.timeout = infra && infra.db && infra.db.timeout ? infra.db.timeout : 5000
    this.Model = Model
  }

  create(object, idColumnName) {
    return new Promise((resolve, reject) => {
      const timeoutObj = this.infra.utility.promiseTimeout(this.timeout, reject, 'Model create operation timeout')
      if (!(object[idColumnName] !== undefined && object[idColumnName] !== '')) {
        object[idColumnName] = this.infra.utility.getTableId()
      }
      object.createdAt = new Date()
      object.updatedAt = new Date()
      let dbObject = new this.Model(object)
      dbObject.put((err, resultObject) => {
        if (err) {
          clearTimeout(timeoutObj)
          reject(this.infra.utility.createError(500, 'DataStoreError', 'create : Error occurred while processing DB operation', err))
        } else {
          clearTimeout(timeoutObj)
          resolve(resultObject)
        }
      })
    })
  }
  findByFilter(filter, options) {
    return new Promise((resolve, reject) => {
      const timeoutObj = this.infra.utility.promiseTimeout(this.timeout, reject, 'Model create operation timeout')
      this.Model.query(filter, options).exec()
        .then((results) => {
          clearTimeout(timeoutObj)
          resolve(results)
        })
        .catch(err => {
          clearTimeout(timeoutObj)
          reject(this.infra.utility.createError(500, 'DataStoreError', 'create : Error occurred while processing DB operation', err))
        })
    })
  }
  findOne(filter) {
    let t = this
    return new Promise((resolve, reject) => {
      const timeoutObj = this.infra.utility.promiseTimeout(this.timeout, reject, 'Model create timeout');
      t.Model.queryOne(filter).exec()
        .then((result) => {
          resolve(result)
        })
        .catch(err => {
          clearTimeout(timeoutObj)
          reject(this.infra.utility.createError(500, 'DataStoreError', 'create : Error occurred while processing DB operation', err))
        })
    })
  }
}

module.exports = (infra) => {
  const dynamoose = require('dynamoose')
  let config = {
    region: infra.awsRegion
  }
  if (infra.db && infra.db.local) {
    dynamoose.local(`http://${infra.db.host}:${infra.db.port}`)
    config.accessKeyId = 'default'
    config.secretAccessKey = 'default'
  }
  dynamoose.AWS.config.update(config)
  return {
    impl: ModelImplementation,
    library: dynamoose
  }
}
