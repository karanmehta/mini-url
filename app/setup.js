'use strict'

const fs = require('fs')
const swaggerTools = require('swagger-tools')
const jsyaml = require('js-yaml')
const cors = require('cors')

class Service {
  constructor(app, infra) {
    this.app = app
    this.pingController = require('./controllers/Ping')
    this.minifyController = require('./controllers/MinifyService')(infra)
  }

  setup() {
    this.app.use(cors())
    const options = {
      controllers: {
        ping: (req, res, next) => {
          this.pingController.pingServer(req, res, next)
        },
        generateShortId: (req, res, next) => {
          this.minifyController.generateShortId(req, res, next)
        },
        retrieveUrl: (req, res, next) => {
          this.minifyController.retrieveUrl(req, res, next)
        },
        redirectTo: (req, res, next) => {
          this.minifyController.redirectTo(req, res, next)
        }
      }
    }
    let spec = fs.readFileSync('./app/api/swagger.yaml', 'utf8')
    let swaggerDoc = jsyaml.safeLoad(spec)
    swaggerTools.initializeMiddleware(swaggerDoc, (middleware) => {
      this.app.use(middleware.swaggerMetadata())
      this.app.use(middleware.swaggerValidator())
      this.app.use(middleware.swaggerRouter(options))
    })
  }
}

module.exports = (app, infra) => {
  const s = new Service(app, infra)
  s.setup()
  return s
}
