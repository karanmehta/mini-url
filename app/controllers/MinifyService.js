'use strict'

class Service {
  constructor(infra) {
    this.db = require('../libs/db')(infra)
    let ModelImplementation = this.db.impl
    this.model = require('../models/Reference')(infra)
    this.refImpl = new ModelImplementation(infra, this.model)
    this.infra = infra
  }
  async generateShortId(req, res, next) {
    try {
      let request = req.swagger.params.body.value
      if (!request || !request.url) {
        throw this.infra.utility.createError(400, 'Bad Request', 'Invalid Parameters')
      }
      if (request.url) {
        let data = await this.refImpl.findByFilter({mapping: request.url}, {})
        if (data && data.length && data[0] && data[0].id) {
          this.infra.utility.sendResponse(null, 201, {shortId: data[0].id}, req, res)
        } else {
          let result = await this.refImpl.create({mapping: request.url}, 'id')
          if (result && result.id) {
            this.infra.utility.sendResponse(null, 201, {shortId: result.id}, req, res)
          } else {
            throw this.infra.utility.createError(500, 'Unexpected error', 'Error while connecting to DB')
          }
        }
      }
    } catch (error) {
      this.infra.logger.debug('Error: ', error)
      this.infra.utility.sendResponse(error, error.code, null, req, res)
    }
  }
  async retrieveUrl(req, res, next) {
    try {
      let shortId = req.swagger.params.shortId.value
      let result = await this.refImpl.findOne({id: shortId})
      if (result && result.mapping) {
        this.infra.utility.sendResponse(null, 200, {url: result.mapping}, req, res)
      } else {
        throw this.infra.utility.createError(404, 'Not Found', 'No records found')
      }
    } catch (error) {
      this.infra.logger.debug('Error: ', error)
      this.infra.utility.sendResponse(error, error.code, null, req, res)
    }
  }
  async redirectTo(req, res, next) {
    try {
      let shortId = req.swagger.params.shortId.value
      let result = await this.refImpl.findOne({id: shortId})
      if (result && result.mapping) {
        this.infra.utility.sendResponse(null, 302, {Location: result.mapping}, req, res)
      } else {
        throw this.infra.utility.createError(404, 'Not Found', 'No records found')
      }
    } catch (error) {
      this.infra.logger.debug('Error: ', error)
      this.infra.utility.sendResponse(error, error.code, null, req, res)
    }
  }
}

module.exports = (infra) => {
  return new Service(infra)
}
