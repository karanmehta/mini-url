'use strict'
const bunyan = require('bunyan')
const bunyanFormat = require('bunyan-format')
class InfraLogger {
  static getLogger(infra) {
    this.options = {}
    this.options['name'] = `${(process.env.APP_ENV || 'local')}:${(infra.appName || '')}`
    this.options['src'] = (infra.logger && infra.logger.srcFlag) || false
    this.options['stream'] = bunyanFormat({outputMode: infra.logger && infra.logger.outputMode ? infra.logger.outputMode : 'json', levelInString: true})
    this.options['level'] = infra.logger && infra.logger.level ? infra.logger.level : 'info'
    if (infra.logger && infra.logger.streams) {
      this.options['streams'] = infra.logger.streams
    }
    return bunyan.createLogger(this.options)
  }
}

module.exports = function (infra) {
  return InfraLogger.getLogger(infra)
}
